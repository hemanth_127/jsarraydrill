function each (elements, cb) {
  
  if (!Array.isArray(elements)) {
    return null
  }

  for (let index = 0; index < elements.length; index++) {
    cb(elements[index], index) ;
  }
}

module.exports = each
