function map (elements, cd) {
    
  if (!Array.isArray(elements)) {
    return
  }

  output = []
  for (let index = 0; index < elements.length; index++) {
    output.push(cd(elements[index], index))
  }
  return output
}

module.exports = map
