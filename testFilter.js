const filter = require('./filter')
const items = require('./items')

function cb (element) {
  if (element % 2 == 0) {
    return 'true'
  } else {
    return 'false'
  }
}

console.log(filter(items, cb))
