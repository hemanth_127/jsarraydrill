function reduce(elements, cb, startingValue=elements[0]) {
    if (!Array.isArray(elements)){
        return null
    }
    let sum=0
    for (let index = 0; index < elements.length; index++) {
        sum+=cb(startingValue,elements[index]);    
    }
    return sum
}
module.exports=reduce