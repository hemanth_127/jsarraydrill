function find (elements, cb) {
  if (!Array.isArray(elements)) {
    return null
  }

  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index]) === 'true') {
      console.log(elements[index])
    } else {
      return undefined
    }
  }
}
module.exports = find
