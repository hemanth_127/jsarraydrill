function flatten (elements) {
  if (!Array.isArray(elements)) {
    return null
  }

  const result = []
  const recursion = arr => {
    for (const element of arr) {
      if (Array.isArray(element)) {
        recursion(element)
      } else {
        result.push(element)
      }
    }
  }

  recursion(elements)
  return result
}
const nestedArray = [1, [2], [[3]], [[[4]]]]
console.log(flatten(nestedArray))
