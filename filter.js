function filter (elements, cb) {

  if (!Array.isArray(elements)) {
    return null
  }

  let output = []  
  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index]) == 'true') {
      output.push(elements[index])
    }
  }
  return output
}
module.exports = filter
